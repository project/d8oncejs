Drupal 8 once.js backport

This is for once.js, not jquery.once.js.

The once.js library is included in core in Drupal 9. However, some Drupal 8
modules have inadvertently made dependencies on once.js. 

Added Drupal 9 compatibility for Upgrade Status module. Attachment added to pages
only when version less than 9.

